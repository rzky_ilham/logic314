--sql Day02

--CAST
--sql Day02

--CAST

select cast (10 as decimal (18,4))
select cast('10' as int )
select CAST(10.65 as int)
select cast ('2023-03-16' as datetime)
select GETDATE(),GETUTCDATE()
select day(getdate())as hari,MONTH(getdate())as Bulan,YEAR(getdate())as Tahun

--convert
select CONVERT (decimal (18,4),10)
SELECT CONVERT(INT,'10')
SELECT CONVERT(INT ,10.65)
SELECT CONVERT (DATETIME,'2023-03-16') jam

--dateAdd
select DATEADD (year,2,getdate())tambah_Tahun,DATEADD(MONTH,3,GETDATE())Tambah_bulan,
DATEADD(day,5,getdate())Tambah_hari

--DateDiff

select DATEDIFF(DAY,'2023-03-16','2023-03-25') as JarakHari,
DATEDIFF(month,'2023-03-16','2024-06-16') as JarakBulan,
DATEDIFF(year,'2023-03-16','2030-03-16')as jarakTahun

--index
create index index_name
on mahasiswa (nama)

--create index index_Address_email
--on mahasiswa(alamat,email)

select * from mahasiswa where nama ='Toni'
select * from mahasiswa

--create unique index
create unique index uniqueindex_alamat
on mahasiswa(alamat)

--drop index
drop index index_alamat_email on mahasiswa -- sama dengan unique index

--add primary key
alter table mahasiswa 
add constraint pk_id_alamat primary key(id,alamat)

--drop primary key
alter table mahasiswa drop constraint pk_alamat

--add unique constrain
alter table mahasiswa 
add constraint unique_alamat unique(alamat)

alter table mahasiswa 
add constraint unique_panjang unique(panjang)
--drop unique key
alter table mahasiswa drop constraint unique_alamat

update mahasiswa set panjang = 59 where id= 23

