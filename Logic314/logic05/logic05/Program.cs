﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //padLeft();
            //contain();
            covertArraysAll();
            Console.ReadKey();
        }
        static void covertArraysAll()
        {
            Console.WriteLine("===covertArraysAll()");
            Console.WriteLine(" Masukkan Angka Array (pakai koma)");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse); 
            Console.WriteLine(string.Join("\t",array));
        }
        static void padLeft() 
        {

            Console.WriteLine("==PadLeft==");
            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());
            Console.Write(" Masukkan Panjang Karakter : ");
            int panjang = int.Parse(Console.ReadLine());
            Console.Write(" Masukkan char : ");
            char chars=char.Parse(Console.ReadLine());

            Console.WriteLine( $"Hasil PadLeft  : {input.ToString().PadLeft(panjang,chars)}");
        }
            static void contain() 
            
            {

                Console.WriteLine(" ===contain===");
                Console.Write(" Masukkan Kalimat : ");
                string kalimat = Console.ReadLine();
                Console.Write(" Masukkan Contain : ");
                string contain = Console.ReadLine();

                if (kalimat.Contains(contain)) 
                {
                    Console.WriteLine($"kalimat ({kalimat}) ini mengandung {contain} ");
                }
                else
            {
                Console.WriteLine($"kalimat ({kalimat})ini tidak mengandung {kalimat} ");
            }
            }
        
    }
}
