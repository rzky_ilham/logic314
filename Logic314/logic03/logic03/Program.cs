﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using System.Transactions;

namespace logic03
{
    internal class Program
    {
        private static int i;

        static void Main(string[] args)
        {
            //modulusTest();
            //pemulung();
            //nilai();
            //genapGanjil();
            //perulanganWhile();
            //angka();
            //PerulanganForincrement();
            //Break();
            //Continue();
            //PerulanganFordecrement();
            //tugas01();
            //pulsa();
            //grab();
            //ForBersarang();
            //array();
            //arrayForeach();
            //arrayFor();
            //array2dimensi();
            //array2dimensiFor();
            //splitJoin();
            subString();
            stringTocharArray();
            Console.ReadKey();
        }
        static void stringTocharArray()
        {
            Console.WriteLine("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();
            foreach (char ch in array) 
            {
                Console.WriteLine(ch);
            
            } 
        
        }
        static void subString() 
        {
            Console.WriteLine("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.WriteLine("Substring(1,4) : " + kalimat.Substring(1,4));
            Console.WriteLine("Substring(5,2) : " + kalimat.Substring(5, 2));
            Console.WriteLine("Substring(7,9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("Substring(9) : " + kalimat.Substring( 9));

        }
        static void splitJoin() 
        {
            Console.WriteLine("==Split & Join==");
            Console.WriteLine("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");

            foreach (string kata in kataArray) 
            {
            Console.WriteLine($"{kata}");
            
            }
            Console.WriteLine(string.Join(",",kataArray));

        }
        static void array2dimensiFor()
        {
            int[,] array = new int[,]
            {
            { 1,2,3},
            { 4,5,6},
            { 7,8,9}
            };

            for (int i = 0; i < array.Length; i++) 
            {
                for (int j = 0; j < 3; j++) 
                {
                    Console.Write(array[i,j] + " ");
                }
                Console.WriteLine();
            }
            
            
            
        }
        static void array2dimensi() 
        {
            int[,] array = new int[,]
            {
            { 1,2,3}, 
            { 4,5,6},
            { 7,8,9}
            };
            Console.WriteLine(array[0,1]);
            Console.WriteLine(array[1,2]);
        }
        static void arrayFor() 
        {
            string[] array = new string[]
                    {
                "abdullah muafa",
                "isni dwiniardi",
                "mario teguh",
                "alfi azizi",
                "alwi sihab"
                    };
            for (int i = 0; i < array.Length; i++);
            {
                Console.WriteLine(array[i]);
            
            }
        }
        static void arrayForeach() 
        {
            string[] array = new string[]
                {
                "abdullah muafa",
                "isni dwiniardi",
                "mario teguh",
                "alfi azizi",
                "alwi sihab"
                };
            foreach (string item in array) 
            {
                Console.WriteLine(item);
            }
        
        }
        

        static void array()
        {
            Console.WriteLine("==Array==");
            int[] staticIntArray = new int[3];
            //isi array
            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;

            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);
        }

        static void ForBersarang() 
        {
            for (int i = 0; i < 3; i++) 
            {
             for(int j = 0; j < 3; j++) 
                {
                Console.Write($"[{i},{j}]");
                }
             Console.WriteLine();
            }
        
        
        }
        static void tugas01()
        {
            Console.WriteLine("Masukkan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int maxNilai = 100;

            if (nilai >= 90 && nilai <= maxNilai)
            {
                Console.WriteLine("Grade A ");

            }
            else if (nilai >= 70 && nilai <= 89)
            {
                Console.WriteLine(" Grade B");
            }
            else if (nilai >= 49 && nilai >= 69)
            {
                Console.WriteLine("Grade C");
            }
            else
            {
                Console.WriteLine("Grade E");
            }

        }

        static void pulsa()
        {
            int pulsa = 100000, point;
            Console.Write("Masukkan Jumlah Pulsa : ");
            pulsa = int.Parse(Console.ReadLine());
            point = 0;
            if (pulsa >= 10000 && pulsa <= 24999)
            {
                point = 80;
            }
            else if (pulsa >= 25000 && pulsa <= 49999)
            {
                point = 200;
            }
            else if (pulsa >= 50000 && pulsa <= 100000)
            {
                point = 800;
            }
            else
            {
                Console.WriteLine("jumlah pulsa tidak sesuai nominal");
            }
            Console.WriteLine($"Point yang didapat adalah : {point}");
        }
        static void grab()
        {
            int belanja, jarak,diskon;
            Console.Write("Total Harga : ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write(" Masukkan Jarak : ");
            jarak= int.Parse(Console.ReadLine());
            int jarakPengiriman = 5;
            Console.Write("Masukkan Kode Promo : ");
            diskon = 30/100;
            string kdpromo = Console.ReadLine();
            int totalpembayaran = belanja;

            if (belanja > 30000)
            {
                belanja -= belanja * diskon;
            }
            else 
            {
                Console.WriteLine(belanja);
            }
            if (kdpromo == "JKTOVO")
            {
                diskon = totalpembayaran * 30 / 100;
                totalpembayaran = belanja - diskon;
            }
         
            
            Console.WriteLine("Total Pembayran Setelah diskon : "+totalpembayaran);

            
            
        
        }

    
        static void Continue()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    continue;
                }
                Console.WriteLine(i);
            }

        }
        static void Break() 
        {
         for(int i = 0; i < 10; i++) 
            {
            if(i==5) 
                {
                    break;
                }
                Console.WriteLine(i);            
            }
        
        }
        static void PerulanganForincrement()
        { 
        for(int i = 0; i < 10; i++) 
            {
              Console.WriteLine(i);
            }
        }
        static void PerulanganFordecrement()
        { 
        for (int i = 10; i > 0; i--) 
            {
            Console.WriteLine(i);
            }
        
        }

        static void modulusTest()
        {
            int has, ang, pem;
         
            Console.Write("Masukkan Angka :");
            ang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Pembagi ");
            pem = int.Parse(Console.ReadLine());
            has = ang % pem;
            if (has == 0) 
            {
                Console.WriteLine("Hasil" + ang + " % " + pem + "adalah" + has);
            
            }
            else 
            {
                Console.WriteLine("Hasil "+ ang + "%"+ pem+ "bukan 0 melainkan " + has);
            }
                      
        }
        static void pemulung()
        {
            int rokok, puntung, h;
            Console.Write("Banyak puntung Rokok : ");
            puntung = int.Parse(Console.ReadLine());
            rokok = 0;
            h = 0;

            if (puntung >= 8)
            {
                rokok = puntung / 8;
                h = puntung % 8;
                int PerbatangRokok = rokok;
                int HargaH = PerbatangRokok * 500;
                Console.WriteLine("Batang rokok hasil rangkai :" + PerbatangRokok);
                Console.WriteLine("Sisa rangkai puntung  rokok adalah :" + h);
                Console.WriteLine("penghasilan dari penjualan rokok :" + HargaH);

            }
            else if (puntung >= 20)
            {
                h = puntung % 8;
                rokok = puntung / 8;
                int PerbatangRokok = rokok;
                int HargaH = PerbatangRokok * 500;

                Console.WriteLine("Batang rokok hasil rangkai : " + PerbatangRokok);
                Console.WriteLine("Sisa rangkai puntung  rokok adalah :" + h);
                Console.WriteLine("Penghasilan dari penjualan rokok : " + HargaH);
            }
            else 
            {
                Console.WriteLine("sorry puntung tidak mencukupi ");
            
            }
        
        
        
        
        }

        static void nilai() 
        {
            Console.WriteLine("Masukkan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int maxNilai = 100;

            if (nilai >= 80 && nilai <= maxNilai)
            {
                Console.WriteLine("Grade A ");

            }
            else if (nilai >= 60 && nilai < 80)
            {
                Console.WriteLine(" Grade B");
            }
            else if (nilai < 60)
            {
                Console.WriteLine("Grade C");
            }
            else 
            {
                Console.WriteLine("Masukkan Angka Yang benar");
            }


        }
        static void genapGanjil()
        {
            int bil;
            Console.Write(" Masukkan Angka : ");
            bil = int.Parse(Console.ReadLine());

            if (bil % 2 == 0)
            {

                Console.WriteLine("Genap");

            }
            else
            {
                Console.WriteLine("Ganjil");
            }

        }

        static void perulanganWhile()
        { 
           Console.WriteLine("===Perulangan While===");
            bool ulangi = true;
            int nilai = 1;

            while (ulangi) 
            {
               Console.WriteLine($"Proses ke : {nilai}");
                nilai++;

                Console.WriteLine($" apakah anda akan mengulang proses ? ( y/n)");
                string input = Console.ReadLine().ToLower();

                if (input == "y")
                {
                    ulangi = true;
                }
                else if (input == "n")
                {
                    ulangi = false;

                }
                else 
                {
                    nilai = 1;
                    Console.WriteLine(" Input yang anda masukkan salah");
                    ulangi = true;
                }

                
            
            }
        
        
        
        }
        static void angka()
        {
            int a = 0;
            do
            {
                Console.WriteLine("a");
                a++;
            }
             while (a < 5);

        }
    }
        

}

