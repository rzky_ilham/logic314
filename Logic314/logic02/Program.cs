﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ifstatment();
            //elsestatment();
            //nestedifstatment();
            //ternary();
            //switchCase();
            Console.ReadKey();
        }

        static void switchCase()
        {
            Console.WriteLine("--SWITCH CASE--");
            Console.WriteLine("Silahkan Pilih buah kesukaan Anda : (apel,mangga,pisang)");
            string input = Console.ReadLine();

            switch (input)
            {
                case "apel":
                    Console.WriteLine("Kamu memilih Apel ");
                    break;
                case "mangga":
                    Console.WriteLine("Kamu memilih Mangga");
                    break;
                case "pisang":
                    Console.WriteLine("Kamu memilih Pisang");
                    break;
                default: 
                    Console.WriteLine("Anda memilih buah yang lain");
                    break;
            }
        }

        

        static void ternary()
        {
            Console.WriteLine("--TERNARY--");
            int x, y;
            Console.WriteLine("Masukkan nilai x :");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Masukkan Nilai y :");
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "Nilai x lebih besar dari y" :
                x < y ? "Nilai x lebih kecil dari y" : "Nilai x dan y sama";

            Console.WriteLine(hasilTernary);
        }
        static void nestedifstatment()
        {
            Console.WriteLine("--IF Nested--");
            Console.WriteLine("Masukkan nilai : ");
            int nilai =int.Parse(Console.ReadLine());
            int maxNilai = 100;

            if(nilai >= 70 && nilai <= maxNilai)
            {
                Console.WriteLine("Selamat kamu berhasil ");
                if(nilai == maxNilai) 
                {
                Console.WriteLine("Kamu Keren !");
                }
            }
            else if (nilai  >= 0 && nilai < 70) 
            {
                Console.WriteLine(" kamu gagal");
            }
            else 
            {
            Console.WriteLine("Masukkan angka yang benar");
            }
                
        
        }
        static void elsestatment()
        {
            Console.WriteLine("---If Statment---");
            int x, y;
            Console.Write("Masukkan Nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilay y : ");
            y = int.Parse(Console.ReadLine());
            if (x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari y");
            }
            else if (x < y)
            {
                Console.WriteLine("Nilai x Lebih kecil dari y");
            }
            else 
            {
                Console.WriteLine("Nilainya sama");
            }

        }
        static void ifstatment() 
        {
            Console.WriteLine("---If Statment---");
            int x,y;
            Console.Write("Masukkan Nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilay y : ");
            y = int.Parse(Console.ReadLine());  

            if(x > y) 
            {
                Console.WriteLine("Nilai x Lebih besar dari y ");
            }
            if(y > x) 
            {
                Console.WriteLine("Nilai y lebih besar dari x");
            }
            if (x==y) 
            {
                Console.WriteLine("nilai x sama dengan y");
            }

        }
    }
}
