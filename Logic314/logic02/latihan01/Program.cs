﻿using System;

namespace latihan01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Lingkaran();
            //persegi();
            Console.ReadKey();
        }
        static void Lingkaran()
        {

            int jari2;
            double keliling,luas;

            Console.WriteLine("--LINGKARAN--");
            Console.Write("Masukkan Jari-jari:");
            jari2=int.Parse(Console.ReadLine());
            
            keliling = 2 * Math.PI * jari2;
            luas = Math.PI * jari2 * jari2;

            Console.WriteLine("Luas Lingkaran " + luas);
            Console.WriteLine("keliling Lingkaran : "+ keliling);
            
            

        }
        static void persegi() 
        {
            int sisi; 
            Console.WriteLine("--PERSEGI--");
            Console.Write("Masukkan Sisi : ");
            sisi = int.Parse(Console.ReadLine());
            
            double luas, keliling;
            
            luas = sisi * sisi;
            keliling = 4 * sisi;

            Console.WriteLine("Luas Persegi : "+ luas);
            Console.WriteLine("Keliling Persegi : " + keliling);
        
        }
    }
}
