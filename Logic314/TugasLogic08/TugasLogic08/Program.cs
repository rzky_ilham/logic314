﻿using System;

namespace TugasLogic08
{
    internal class Program
    {
        static void Main(string[] args)
        {

           Soal_2();

            Console.ReadKey();
        }

        static void Soal_2() 
        {
            int n = 15; // jumlah kue Pukis dalam resep awal
            int terigu = 115; // gram
            int gula = 190; // gram
            int susu = 100; // mL

            Console.Write("Masukkan jumlah kue Pukis yang ingin dibuat: ");
            n = int.Parse(Console.ReadLine());

            double teriguTotal = (double )terigu / 15 * n;
            double gulaTotal = (double)gula / 15 * n;
            double susuTotal = (double)susu / 15 * n;

            Console.WriteLine($"Bahan yang dibutuhkan untuk membuat {n} kue Pukis:");
            Console.WriteLine($"{teriguTotal} gram terigu");
            Console.WriteLine($"{gulaTotal} gram gula pasir");
            Console.WriteLine($"{susuTotal} mL susu");

        }
    }
}
