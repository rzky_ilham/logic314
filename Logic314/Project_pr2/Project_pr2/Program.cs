﻿using System;
using System.Net;

namespace Project_pr2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //tugas01();
            tugas02();
            //tugas03();
            //tugas04();
            //tugas05();
            //tugas06();
            //tugas07();
            //tugas08();
            //tugas08_2();
            //tugas09();
            //tugas10();
            //tugas11();
            //tugas12();
        }
        static void tugas01()
        {
            
            Console.WriteLine("Soal NO.1");
          
            //variabel golongan digunakan untuk menyimpan golongan karyawan yang di input.
                int golongan, jamKerja,jamNormal = 0, jamLembur = 0;
                double upahPerJam = 0, gajiMingguan = 0, upahLembur = 0, upahMingguan = 0;
            //jam kerja menyimpan jumlah jam kerja karyawan pe minggu yang diinput
            //jam normal dan lembur digunakan untuk menyimpan jumlah jam kerja normal dan lembur
            //UpahPerjam menyimpan upah karyawan sesuai dengan golongan
            //gaji mingguan unutk menyimpan jumlah gaji karyawan dalam satu minggu
                Console.Write("Masukkan golongan karyawan (1-4): ");
                golongan = int.Parse(Console.ReadLine());


                if (golongan == 1)
                {
                    upahPerJam = 2000;
                }
                else if (golongan == 2)
                {
                    upahPerJam = 3000;
                }
                else if (golongan == 3)
                {
                    upahPerJam = 4000;
                }
                else if (golongan == 4)
                {
                    upahPerJam = 5000;
                }
                else
                {
                    Console.WriteLine("Golongan karyawan tidak valid.");
                }

                Console.Write("Masukkan jumlah jam kerja per minggu: ");
                jamKerja = int.Parse(Console.ReadLine());

                if (jamKerja > 40)
                {
                    jamNormal = 40;
                    upahMingguan = jamNormal * upahPerJam;
                    jamLembur = jamKerja - jamNormal;
                    upahLembur = upahPerJam * 1.5 * jamLembur;
                    gajiMingguan = (jamNormal * upahPerJam) + upahLembur;
                }
                else
                {
                    upahMingguan = jamKerja * upahPerJam;
                    gajiMingguan = jamKerja * upahPerJam;

                }
                Console.WriteLine("Upah : " + upahMingguan);
                Console.WriteLine("lembur : " + upahLembur);
                Console.WriteLine("Total gaji: " + gajiMingguan);
        }

        static void tugas02()
        {
            Console.WriteLine("Tugas NO.2");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            string[] kata = kalimat.Split(" ");

            for (int i = 0; i < kata.Length; i++)
            {

                Console.WriteLine($"kata{i + 1}= {kata[i]}");
            }

            Console.WriteLine($"Total Kata adalah :  = {kata.Length}");
        }
        static void tugas03()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine();

            string[] kata = kalimat.Split(' ');
            int totalKata = kata.Length;

            for (int i = 0; i < totalKata; i++)
            {

                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kata[i][j]);
                    }
                    else if (j == kata[i].Length - 1)
                    {
                        Console.Write(kata[i][j]);
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.Write(" ");
            }
        }
        static void tugas04()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine();

            string[] kata = kalimat.Split(' ');
            int totalKata = kata.Length;

            for (int i = 0; i < totalKata; i++)
            {
                for (int j = 0; j < kata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write("*");
                    }
                    else if (j == kata[i].Length - 1)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(kata[i][j]);
                    }
                }
                Console.Write(" ");
            }
        }

        static void tugas05()
        {
            Console.WriteLine("==Menghapus Huruf Depan==");
            Console.Write("Masukkan kalimat = ");
            string[] kalimat = Console.ReadLine().Split(" ");
            
            //string[] kata = kalimat.Split(' ');
            int totalKata = kalimat.Length;
            string tampung = "";
            for (int i = 0; i < totalKata; i++)
            {
                char[] chars = kalimat[i].ToCharArray();
                for (int j = 0; j < chars.Length; j++)
                {

                    if (j > 0)
                    {
                        tampung += chars[j];
                        if (j == chars.Length -1) 
                        {
                            tampung += " ";
                        }
                    }
                    
                }
                //tampung += " ";
            }
            Console.WriteLine(tampung);
        }
        static void tugas05_2()
        {
            Console.WriteLine("==Menghapus Huruf Depan==");
            Console.Write("Masukkan kalimat = ");
            string[] kalimat = Console.ReadLine().Split(" ");

            //string[] kata = kalimat.Split(' ');
            int totalKata = kalimat.Length;

            for (int i = 0; i < totalKata; i++)
            {
                Console.Write(kalimat[i].Remove(0, 1));
                Console.Write(" ");
            }
        }
        static void tugas06()
        {
            Console.Write("Masukkan N = ");
            int n = int.Parse(Console.ReadLine());

            int[] arr;
            arr = new int[n];
            int sum = 1;

            for (int i = 0; i < arr.Length; i++)
            {
                sum *= 3;
                arr[i] = sum;
                int nilai = i % 2;
                if (nilai == 0)
                {
                    Console.Write($"{arr[i]} ");
                }
                else
                {
                    Console.Write("* ");
                }
            }
        }
        static void tugas07()
        {
            //
        }

        static void tugas08()
        {
            Console.WriteLine("Masukkan N : ");
            int n = int.Parse(Console.ReadLine());
            int a = 1, b = 1, c;
            Console.Write(a + "," + b);

            for (int i = 2; i < n; i++)
            {
                c = a + b;
                Console.Write("," + c);
                a = b;
                b = c;
            }

            Console.WriteLine();
        }
        static void tugas08_2() 
        {
            int number;
            Console.Write("Masukkan Banyak Angka yang di munculkan : ");
            number = int.Parse(Console.ReadLine());

            int[]numberArray = new int[number];
            for (int i = 0; i< number; i++ ) 
            {
                if (i <= 1)
                {
                    numberArray[i] = 1;
                }
                else 
                {
                    numberArray[i] = numberArray[i - 2] + numberArray[i - 1];
                
                }
            }
            Console.Write(string.Join(",",numberArray));
        
        }
        static void tugas09()
        {
            Console.Write("Masukkan format jam = ");
            string formatJam = Console.ReadLine().ToUpper();
            int jam = int.Parse(formatJam.Substring(0, 2));
            string pm = formatJam.Substring(8, 2);

            if (jam  <= 12)
            {
                if (pm == "PM")
                {
                    jam += 12;
                    Console.WriteLine(jam.ToString() + formatJam.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine(formatJam.Substring(0,8));
                }
                
            }
            else
            {
                Console.WriteLine("Input yang Bener!");
            }
        }

        static void tugas10()
        {
            string merkBaju;
            int hargaBaju = 0;
            string kodeUkuran = "";

            ulang:
            Console.Write("Masukkan Kode Baju = ");
            int kodeBaju = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Kode Ukuran (S, M, L, XL, XXL)= ");
            kodeUkuran = Console.ReadLine().ToLower();

            if (kodeBaju == 1)
            {
                merkBaju = "IMP";

                if (kodeUkuran == "s")
                {
                    hargaBaju = 200000;
                }
                else if (kodeUkuran == "m")
                {
                    hargaBaju = 220000;
                }
                else if (kodeUkuran == "l" || kodeUkuran == "xl" || kodeUkuran == "xxl")
                {
                    hargaBaju = 250000;
                }
                else
                {
                    Console.WriteLine("Inputan anda salah.");
                }

            }
            else if (kodeBaju == 2)
            {
                merkBaju = "Prada";

                if (kodeUkuran == "s")
                {
                    hargaBaju = 150000;
                }
                else if (kodeUkuran == "m")
                {
                    hargaBaju = 160000;
                }
                else
                {
                    hargaBaju = 170000;
                }
            }
            else if (kodeBaju == 3)
            {
                merkBaju = "Gucci";

                if (kodeUkuran == "s" || kodeUkuran == "m" || kodeUkuran == "l" || kodeUkuran == "xl" || kodeUkuran == "xxl")
                {
                    hargaBaju = 200000;
                }
                else
                {
                    Console.WriteLine("Inputan anda tidak valid, ulangi lagi.");
                }
            }
            else
            {
                Console.WriteLine("Inputan tidak valid");
                goto ulang;
            }

            Console.WriteLine($"Merk Baju : {merkBaju}");
            Console.WriteLine($"Harga Baju : {hargaBaju}");
        }

        static void tugas11()
        {
            Console.Write("Masukkan Uang Andi : ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan harga baju (pakai koma) : ");
            int[] hargaBajuArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan Harga Celana (pakai koma) : ");
            int[] hargaCelanaArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;
            for (int i = 0; i < hargaBajuArray.Length; i++)
            {
                for (int j = 0; j < hargaCelanaArray.Length; j++)
                {
                    int harga = hargaBajuArray[i] + hargaCelanaArray[j];
                    if (harga <= uang && harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                    }
                }
            }
            Console.WriteLine(maxBelanja);
            Console.WriteLine($"Sisa Uang anda adalah : {uang - maxBelanja}");
        }
        static void tugas12()

        {
            int angka;

            Console.Write("Masukkan angka: ");
            angka = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int[] arr = new int[angka];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = i + 1;
            }

            for (int j = 0; j < arr.Length; j++)
            {
                for (int k = 0; k < arr.Length; k++)
                {
                    if (j == 0)
                    {
                        Console.Write($"{arr[k]} ");
                    }
                    else if (j == arr.Length - 1)
                    {
                        Console.Write($"{arr[arr.Length - 1 - k]} ");
                    }
                    else
                    {
                        if (k == 0)
                        {
                            Console.Write("* ");
                        }
                        else if (k == arr.Length - 1)
                        {
                            Console.Write("* ");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                }
                Console.WriteLine(" ");
            }
        }

        static void tugas12_2() 
        {
        
        
        }



    }
}
