﻿using System;
using System.Collections.Generic;
namespace logic07
{
    class mamalia 
    {
        public virtual void pindah() 
        {
            Console.WriteLine(" Kucing Lari. . .");
        }
    }
    class Kucing : mamalia//kucing masuk ke class mamalia atau turunan
    {
    
    }
    class Paus : mamalia
    {
        public override void pindah()//untuk merePlace dari lari ke berenang
        {
            Console.WriteLine("Paus Berenang. . . ");
        }
    }
    public class Program
    {
        static readonly List<User> listUser = new List<User>();//tidak harus memakai readOnly
        static void Main(string[] args)
        {

            //bool ulangi = true;
            //while (ulangi) 
            //{ 
            //    InsertUser();
            //}
            //ListUserMethod();
            //dateTime();
            //StringdateTime();
            //timeSpan();
            //ClassInheritence();
            overRidding();
             

            Console.ReadKey();
        }
        static void overRidding() 
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }
        static void ClassInheritence() 
        {
            TypeMobil typeMobil = new TypeMobil();//class dari type mobil
            typeMobil.kecepatan = 100;
            typeMobil.posisi = 50;
            typeMobil.civic();//kalau diatas ga akan ke print

        }
        
        static void timeSpan() 
        {
            DateTime date1 = new DateTime(2023, 1, 1, 1, 0, 0);
            DateTime date2 =  DateTime.Now;
            TimeSpan span = date2-date1;

            Console.WriteLine($"Total Hari :{span.Days}");
            Console.WriteLine($"Total Hari :{span.TotalDays}");
            Console.WriteLine($"Total jam : {span.Hours}");
            Console.WriteLine($"Total jam : {span.TotalHours}");
            Console.WriteLine($"Total Menit :{span.TotalMinutes}");
            Console.WriteLine($"Total Detik :{span.TotalSeconds}");

        }
        static void StringdateTime()
        {
            Console.WriteLine("==String Date Time==");
            Console.Write("Masukkan tanggal (dd/mm/yyyy) : ");
            string strtanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strtanggal);

            Console.WriteLine($" Tanggal : {tanggal.Day}");
            Console.WriteLine($" Bulan  : {tanggal.Month}");
            Console.WriteLine($" Tahun : {tanggal.Year}");
            Console.WriteLine($" DayOfweek ke : {(int)tanggal.DayOfWeek }");
            Console.WriteLine($" DayOfWeek ke :{tanggal.DayOfWeek}");




        }
        static void dateTime() 
        {
            Console.WriteLine("==Date Time==");
            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);

            DateTime dt2 =DateTime.Now;
            Console.WriteLine(dt2);

            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 3, 9);
            Console.WriteLine(dt5);

            //DateTime dt6 =new DateTime(2023, 3, 9, 11, 45);
            //Console.WriteLine(dt6); 
        }

        static void ListUserMethod()
        {
            Console.WriteLine("==List User Method==");
            List<User>listUser = new List<User>()
            {
                new User() {Nama = " Firdha", Umur = 24, Alamat = "Tangsel" },
                new User() {Nama = " isni", Umur = 22, Alamat = "Cimahi" },
                new User() {Nama = " Asti", Umur = 23, Alamat = "Garut" },
                new User() {Nama = " Muafa", Umur = 22, Alamat = "Bogor" },
                new User() {Nama = " Toni", Umur = 24, Alamat = "Garut " },

            };
            
            for (int i = 0; i< listUser.Count; i++) 
                {
                    Console.WriteLine($"Nama {listUser[i].Nama}\tUmur : {listUser[i].Umur}\talamat{listUser[i].Alamat} : ");
                }
            
        
        }
        static void InsertUser()
        {
            Console.WriteLine("==List User Method==");
            Console.Write("Masukkan Nama :");
            string nama = Console.ReadLine();
            Console.Write("Masukkan Umur :");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Alamat :");
            string alamat = Console.ReadLine();

            //List<User> listUser = new List<User>()
            

            User user = new User();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;

            listUser.Add(user);
            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine($"Nama : {listUser[i].Nama}\t Umur : {listUser[i].Umur}\talamat : {listUser[i].Alamat}  ");
            }

        }
    }
}
