﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logic07
{
    public abstract class CalculatorAbstract //kalau absctact langsung memakai kurung kurawal
    {
        public abstract int jumlah(int x, int y);
        public abstract int kurang(int x, int y);

    }
    public class TesTurunan : CalculatorAbstract
    {
        public override int jumlah(int x, int y)
        {
            return x + y;
        }

        public override int kurang(int x, int y)
        {
            return x - y;
        }
    }
}
