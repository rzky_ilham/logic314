﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logic07
{
    internal class User
    {
        public string Nama { get; set; }
        public int Umur { get; set; }
        public string Alamat { get; set; }
    }
}
