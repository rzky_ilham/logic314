﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logic06
{
    public class Mobil//class
    {
        public double kecepatan;//menggunakan public agar bisa diakses diluar kelas
        public double bensin;
        public double posisi;//ini propertis
        private string nama;
        private string platno;

        public Mobil() 
        {
        
        }

        public Mobil(string platno) //INI CONTRACTORS nama di string harus beda dengan menambah _
        {
            this.platno = platno;//bisa ditambah dengan this.
        }//constructor biasanya diatruh di atas

        public void percepat()//INI method(fungsi)
        {
            this.kecepatan += 10;
            this.bensin -=5;
        }
        public void maju() 
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }
        public void isiBensin(double bensin) 
        {
            this.bensin += bensin;
        }

        public string getPlatno()
        { 
            return platno;
        }
    }
}
