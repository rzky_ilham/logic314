﻿using System;
using System.Collections.Generic;

namespace logic06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //list();
            //contohClass();
            //listClass();
            //listAdd();
            listRemove();


            Console.ReadKey();
        }
        static void contohClass() 
        {
            //untuk memanggil objek harus ada contractor
            Mobil mobil = new Mobil("RI SATU");//mengisi constractor

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);//menjalankan fungsi dengan parameter

            string platno = mobil.getPlatno();//mengambil value dari fungsi ,merupakan method return type

            Console.WriteLine($"Plat Nomor : {platno}");
            Console.WriteLine($"Bensin : {mobil.bensin}");
            Console.WriteLine($"Kecepatajn :{mobil.kecepatan}");
            Console.WriteLine($"Posisi : {mobil.posisi}");
        }
        static void listRemove()
        {
            Console.WriteLine("==LIST Remove==");
            List<User> listUser = new List<User>()
            {

            new User(){Name = "isni dwitiniardi",umur= 22},
            new User(){Name = "Astika ",umur = 23},
            new User(){Name ="Alfi azizi",umur=23}

            };
            
            listUser.RemoveAt(2);

            for (int i = 0; i < listUser.Count; i++) //ccount = propertis
            {
                Console.WriteLine(listUser[i].Name + " sudah Berumur " + listUser[i].umur + " tahun");
            }
        }

        static void list()
        {
            Console.WriteLine("--List--");
            List<string >list = new List<string>()
            { 
                "Astika",
                "Marcelino",
                "Alwi Fadli",
                "Toni"
            };
            Console.WriteLine(string.Join(", " , list));
        }
        static void listAdd() 
        {
            Console.WriteLine("==LIST ADD==");
            List<User> listUser = new List<User>()
            {

            new User(){Name = "isni dwitiniardi",umur= 22},
            new User(){Name = "Astika ",umur = 23},
            new User(){Name ="Alfi azizi",umur=23}

            };
            listUser.Add(new User() 
            {Name = "Anwar" ,umur = 22});

            for (int i = 0; i < listUser.Count; i++) //ccount = propertis
            {
                Console.WriteLine(listUser[i].Name + " sudah Berumur " + listUser[i].umur + " tahun");
            }
        }
        static void listClass() 
        {
            Console.WriteLine("==listclasss==");
            List<User>  listUser = new List<User>()
            { 
            
            new User(){Name = "isni dwitiniardi",umur= 22},
            new User(){Name = "Astika ",umur = 23},
            new User(){Name ="Alfi azizi",umur=23}
            
            };

            for (int i = 0; i < listUser.Count; i++) //ccount = propertis
            {
                Console.WriteLine(listUser[i].Name + " sudah Berumur "+ listUser[i].umur + " tahun");
            }

        
        }
    }
}
