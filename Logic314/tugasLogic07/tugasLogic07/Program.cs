﻿using System;

namespace tugasLogic07
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Soal01();
            Soal02();
            //Soal03();
            //Soal04();
            //Soal05();
            //Soal06();
            //Soal07();
            //Soal08();
            //Soal09();
            //Soal10();
            //Soal11();
            //Soal12();
            Console.ReadKey();
        }
        static void Soal01()
        {
            int angka, faktorial = 1;
            Console.Write("Masukkan Angka = ");
            angka = int.Parse(Console.ReadLine());

            Console.Write("{0}! = ", angka);

            while (angka > 0)
            {
                faktorial *= angka;
                Console.Write(angka);
                angka--;
                if (angka > 0)
                {
                    Console.Write(" x ");
                }
            }
            Console.Write(" = " + faktorial);
        }

        static void Soal02()
        {
            //SORSPQSPS
            Console.Write("Masukkan Sinyal = ");
            string sinyal = Console.ReadLine().ToUpper();

            int jumlah = 0;
            for (int i = 0; i < sinyal.Length; i += 3)
            {
                if (sinyal[i] != 'S')
                {
                    jumlah++;
                }
                if (sinyal[i + 1] != 'O')
                {
                    jumlah++;
                }
                if (sinyal[i + 2] != 'S')
                {
                    jumlah++;
                }
            }
            Console.Write("Total yang salah = " + jumlah);
        }

        static void Soal03()
        {
            Console.Write("Tanggal Peminjaman Buku = ");
            DateTime tanggalPinjam = DateTime.Parse(Console.ReadLine());
            Console.Write("Tanggal Kembali Buku = ");
            DateTime tanggalKembali = DateTime.Parse(Console.ReadLine());

            int lamaPinjam = 3;
            int dendaPerHari = 500;

            TimeSpan selisih = tanggalKembali - tanggalPinjam;
            int hariTerlambat = (int)selisih.TotalDays - lamaPinjam;

            if (hariTerlambat > 0)
            {
                int totalDenda = hariTerlambat * dendaPerHari;
                Console.WriteLine("Mono harus membayar denda sebesar " + totalDenda + " rupiah.");
            }
            else
            {
                Console.WriteLine("Mono tidak dikenakan denda karena pengembalian tepat waktu.");
            }
        }

        static void Soal04()
        {
            Console.Write("Tanggal Mulai (dd/mm/yyyy) = ");
            DateTime tanggalMulai = DateTime.Parse(Console.ReadLine());
            Console.Write("Tanggal Libur (pakai koma) = ");
            int[] tanggalLibur = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int durasiKelas = 10;

            DateTime akhirKelas = tanggalMulai.AddDays(durasiKelas - 1); // tanggal akhir kelas (tanpa memperhitungkan libur)
            int totalLibur = 0;

            // perhitungan jumlah hari libur
            foreach (int tglLibur in tanggalLibur)
            {
                DateTime libur = new DateTime(2022, 9, tglLibur);
                if (libur >= tanggalMulai && libur <= akhirKelas && libur.DayOfWeek != DayOfWeek.Saturday && libur.DayOfWeek != DayOfWeek.Sunday)
                {
                    totalLibur++;
                }
            }

            // penyesuaian tanggal akhir kelas berdasarkan jumlah hari libur
            akhirKelas = akhirKelas.AddDays(-totalLibur);

            // penentuan tanggal ujian FT1
            DateTime ft1Date = akhirKelas.AddDays(1);

            // penyesuaian tanggal ujian FT1 jika jatuh pada akhir pekan
            if (ft1Date.DayOfWeek == DayOfWeek.Saturday)
            {
                ft1Date = ft1Date.AddDays(2);
            }
            else if (ft1Date.DayOfWeek == DayOfWeek.Sunday)
            {
                ft1Date = ft1Date.AddDays(1);
            }

            Console.WriteLine("Kelas akan ujian pada = " + ft1Date.ToString("dd/MM/yyyy"));
        }

        static void Soal05()
        {
            Console.Write("Masukkan kalimat = ");
            string kalimat = Console.ReadLine().ToLower();

            int vokal = 0, konsonan = 0;
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (kalimat[i] == 'a' || kalimat[i] == 'i' || kalimat[i] == 'u' || kalimat[i] == 'e' || kalimat[i] == 'o')
                {
                    vokal++;
                }
                else if (char.IsWhiteSpace(kalimat[i]))
                {
                    continue;
                }
                else
                {
                    konsonan++;
                }

            }
            Console.WriteLine("Jumlah huruf vokal = " + vokal);
            Console.WriteLine("Jumlah huruf konsonan = " + konsonan);
        }

        static void Soal06()
        {
            Console.Write("Masukkan nama = ");
            string input = Console.ReadLine();

            string starKiri = "";
            string starKanan = "";
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input.Length; j++)
                {
                    if (j == 0)
                    {
                        starKiri = "***";
                    }

                    if (j == i)
                    {
                        starKanan = "***";
                    }
                }
                Console.WriteLine(starKiri + input[i] + starKanan);
            }
        }

        static void Soal07()
        {
            Console.Write("Masukkan total menu              = ");
            int totalMenu = int.Parse(Console.ReadLine());
            Console.Write("Masukkan index makanan alergi    = ");
            int indexMakananAlergi = int.Parse(Console.ReadLine());
            Console.Write("Masukkan harga menu              = ");
            int[] hargaMenu = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan uang Elsa               = ");
            int uang = int.Parse(Console.ReadLine());

            int totalHargaMenu = 0;
            int bisaDiMakan = 0;
            int totalBayar = 0;
            int sisa = 0;

            for (int i = 0; i < hargaMenu.Length; i++)
            {
                totalHargaMenu += hargaMenu[i];
            }

            bisaDiMakan = totalHargaMenu - hargaMenu[indexMakananAlergi];
            totalBayar = bisaDiMakan / 2;
            sisa = uang - totalBayar;

            if (sisa > 0)
            {
                Console.WriteLine($"Elsa Harus Membayar = " + totalBayar.ToString("Rp #,##"));
                Console.WriteLine($"Sisa Uang Elsa = " + sisa.ToString("Rp #,##"));
            }
            else if (sisa == 0)
            {
                Console.WriteLine("Uang Pas");
            }
            else
            {
                Console.WriteLine(sisa.ToString("Rp #,##"));
            }
        }

        static void Soal08()
        {
            Console.Write("Masukkan baris angka = ");
            int baris = int.Parse(Console.ReadLine());

            for (int i = 1; i <= baris; i++)
            {
                for (int j = baris - i; j >= 1; j--)
                {
                    Console.Write(" ");
                }

                for (int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("\n");
            }
        }

        static void Soal09()
        {
            int[,] matrix = new int[,]
            {
                { 11, 2, 4 },
                { 4, 5, 6 },
                { 10, 8, -12 }
            };

            int primaryDiagonalSum = 0;
            // menghitung jumlah elemen diagonal utama dengan melakukan iterasi pada baris dan kolom yang sama (matrix[i, i])
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                primaryDiagonalSum += matrix[i, i];
            }

            // Menghitung diagonal samping
            int secondaryDiagonalSum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                // menghitung jumlah elemen diagonal samping dengan melakukan iterasi pada baris yang sama dan kolom yang berlawanan
                secondaryDiagonalSum += matrix[i, matrix.GetLength(0) - 1 - i];
            }

            // Menghitung perbedaan diagonal
            int diagonalDifference = primaryDiagonalSum - secondaryDiagonalSum;

            // Menampilkan hasil output
            Console.WriteLine("Perbedaan Diagonal = " + diagonalDifference);
        }

        static void Soal10()
        {
            Console.Write("Masukkan inputan angka = ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int hitung = 0;
            int terbesar = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > terbesar)
                {
                    terbesar = arr[i];
                    hitung = 1;
                }
                else if (arr[i] == terbesar)
                {
                    hitung++;
                }

            }
            Console.WriteLine(hitung);
        }

        static void Soal11()
        {
            Console.Write("Masukkan input angka (pakai koma) = ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan rotasi = ");
            int rotasi = int.Parse(Console.ReadLine());

            // melakukan rotasi pada array sebanyak n kali
            for (int i = 0; i < rotasi; i++)
            {
                // menyimpan elemen pertama dari array ke dalam variabel tampung
                int tampung = arr[0];

                // menggeser setiap elemen array ke kiri sebanyak satu indeks
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    arr[j] = arr[j + 1];
                }

                // memasukkan nilai tampung ke dalam indeks terakhir array
                arr[arr.Length - 1] = tampung;
            }

            Console.Write($"Rotasi {rotasi} = ");
            // menampilkan hasil rotasi array
            for (int i = 0; i < arr.Length; i++)
            {
                // menampilkan setiap elemen array
                Console.Write(arr[i]);

                // jika elemen tersebut bukan elemen terakhir dari array, maka program juga menampilkan tanda koma
                if (i != arr.Length - 1)
                {
                    Console.Write(",");
                }
            }
        }

        static void Soal12()
        {
            Console.Write("Masukkan jumlah bilangan: ");
            int n = int.Parse(Console.ReadLine());

            Console.Write("Masukkan bilangan, dipisahkan dengan koma: ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);

            //int[] arr = Array.ConvertAll(inputArr, int.Parse);

            // Bubble Sort
            for (int i = 0; i < arr.Length - 1; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        int temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }

            Console.Write("Hasil Bubble Sort: ");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]);
                if (i != arr.Length - 1)
                {
                    Console.Write(",");
                }
            }
        }

        }
}
