﻿using System;
using System.ComponentModel.Design;
using System.Net;
using System.Xml.Schema;

namespace Logic07_pr
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //udah//Soal_1();
            //Soal_2();
            //Soal_3();
            //Soal_4();
            //Soal_5();
            Soal_52();
            //Soal_8();
            //Soal_6();
            //Soal_7();

            Console.ReadKey();
        }
        static void Soal_3()
        {
            Console.WriteLine(" Tanggal Peminjaman (dd/mm/yyyy): ");
            string Tanggalpinjam = Console.ReadLine();

            Console.WriteLine("Tanggal Pengembalian (dd/mm/yyyy): ");
            string TanggalKembali = Console.ReadLine();

            DateTime dateTanggalP = DateTime.Parse(Tanggalpinjam);
            DateTime dateTanggalK = DateTime.Parse(TanggalKembali);

            int denda = 0;

            TimeSpan span = dateTanggalK.Subtract(dateTanggalP);

            if (span.Days > 3)
            {
                denda = (span.Days - 3) * 500;
                Console.WriteLine($"Anda terlambat mengembalikan buku selama {span.Days - 3} hari");
                Console.WriteLine($"Denda yang harus di bayar {denda}");

            }
            else
            {
                Console.WriteLine("Terimakasih anda telah mengembalikan buku tepat waktu");
            }

        }
        static void Soal_4()
        {
            Console.Write("Masukkan Tanggal Mulai (dd/mm/yyyy) :");
            string tanggalMulai = Console.ReadLine();
            Console.Write("Hari libur : ");

            int durasikelas = 10;






            Console.Write($"Kelas akan Dimulai pada : ");


        }
        static void Soal_6()
        {
            Console.WriteLine("Masukkan Nama : ");
            string kalimat = Console.ReadLine();
            string[] kata = kalimat.Split(' ');
            foreach (char tes in kalimat)//melooping data yang sudah ada pada array
            //for (int i =0; i < kata.Length; i++) 
            {
                Console.WriteLine($"***{tes}***");
            }

        }



        static void Soal_8()
        {
            Console.Write(" Masukkan Inputan : ");
            int angka = int.Parse(Console.ReadLine());

            for (int i = 1; i <= angka; i++)
            {
                //for (int j = 1; j <= angka - i; j++)//kanan ke kiri
                for (int j = angka; j < 1; j--)//kiri ke kanan
                {
                    Console.Write(" ");
                }
                for (int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
        static void Soal_52() 
        {
            Console.Write("Masukkan Kata : ");
            string kata = Console.ReadLine();
            
            int vokal = 0;
            int konsonan = 0;
            for (int i =0; i < kata.Length; i++)
            {
                if (kata[i] == 'a' || kata[i] == 'A' || kata[i] == 'i' || kata[i] == 'I' || kata[i] == 'u' || kata[i] == 'U' || kata[i] == 'e' || kata[i] == 'E' ||
                    kata[i] == 'o' || kata[i] == 'O')

                {
                    vokal ++;
                }
                else 
                {
                    konsonan++;
                }
                
            }
            Console.WriteLine($"Jumlah huruf Vokal adalah : " + vokal);
            Console.WriteLine($"Jumlah huruf konsonan adalah : " + konsonan);

        }
        static void Soal_5()
        {
            Console.Write("Masukan kalimat : ");
            string kalimat = Console.ReadLine();
            int jumvokal = 0;
            int jumkonsonan = 0;
            foreach (char kata in kalimat)
            {
                if (char.IsLetter(kata))
                {
                    if ("aiueoAIUEO".IndexOf(kata) != -1)
                    {
                        jumvokal++;
                    }
                    else
                    {
                        jumkonsonan++;
                    }

                }
            }
            Console.WriteLine("Jumlah Vokal: " + jumvokal);
            Console.WriteLine("Jumlah konsonan: " + jumkonsonan);

        }
        

        static void Soal_1()
        {

            Console.WriteLine("==FAKTORIAL!==");
            Console.WriteLine(" banyak anak : ");
            int anak = int.Parse(Console.ReadLine());

            Console.Write($"{anak}! = ");
            int hasil = 1;
            for (int i = 1; i <= anak; i++)
            {
                hasil = hasil * i;
                Console.Write(i);
                if (i != anak)
                {
                    Console.Write("*");
                }
                else
                {
                    Console.Write($" = {hasil}");
                }
            }
            Console.WriteLine($"\n ada {hasil} cara");


        }
        static void Soal_2()
        {
            Console.Write(" Masukkan sinyal : ");
            string kalimat = Console.ReadLine().ToUpper(); 
            char[] karakter = kalimat.ToCharArray();

            int sinyalBenar = 0;
            int sinyalsalah = 0;
            Console.WriteLine(" Sinyal Yang benar : SOSSOSSOSSOS");

            for (int i = 0; i < karakter.Length; i += 3)
            {
                if (karakter[i] == 'S' && karakter[i + 1] == 'O' && karakter[i + 2] =='S')
                {
                    sinyalBenar += 1;
                }
                if (karakter[i] == 'S' || karakter[i + 1] == 'O' || karakter[i + 2] == 'S') 
                {
                    sinyalsalah += 1;
                }


            }
                Console.WriteLine($"Jumlah Sinyal salah :" +sinyalsalah);
                
        }
    }

    
}
