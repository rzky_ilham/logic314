﻿using System;

namespace logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //logika();
            //konversi();
            //perbandingan();
            //aritmatika();
            //penugasan();
            //modulus();
            //logika();
        }
        static void konversi()
        {
            int umur = 19;
            string strUmur = umur.ToString();

            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = false;
            Console.WriteLine("===Konversi===");
            Console.WriteLine(strUmur);
            Console.WriteLine(Convert.ToString(myInt)); // conveert int ke string
            Console.WriteLine(Convert.ToDouble(myInt)); // convert int ke double
            Console.WriteLine(Convert.ToInt32(myDouble)); //convert double ke int
            Console.WriteLine(Convert.ToString(myBool)); //convert bool ke string
        }
        static void perbandingan()
        {
            int mangga, apel = 0;
            Console.Write("jumlah mangga ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("jumlah apel");
            apel = int.Parse(Console.ReadLine());


            Console.WriteLine("hasil perbandingan : ");
            Console.WriteLine($"mangga > apel : {mangga} > {apel}");
            Console.WriteLine($"mangga >= apel : {mangga} >= {apel}");
            Console.WriteLine($"mangga < apel : {mangga} < {apel}");
            Console.WriteLine($"mangga <= apel : {mangga} <= {apel}");
            Console.WriteLine($"mangga == apel : {mangga} == {apel}");
            Console.WriteLine($"mangga =! apel : {mangga} =! {apel}");
        }
        static void aritmatika()
        {

            int mangga, apel;
            Console.Write("Jumlah Mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("jumlah apel = ");
            apel = int.Parse(Console.ReadLine());

            int hasil = HitungJumlah(mangga, apel);

            Console.WriteLine($"hasil mangga + apel = {hasil}");
        }
        static int HitungJumlah(int mangga, int apel)
        {
            int hasil = 0;
            hasil = mangga + apel;  
            return hasil;

        }

        static void penugasan()
        {
            Console.WriteLine("--Operator Penugasan--");
            int mangga = 10;
            int apel = 8;

            mangga = 15;
            Console.WriteLine($"mangga = {mangga}");

            apel = 8;
            Console.WriteLine($"apel = {apel}");


            apel += mangga;
            Console.WriteLine($"apel = {apel}");

            mangga = 15;
            apel = 8;
            apel -= mangga;
            Console.WriteLine($"apel= {apel}");

            mangga = 15;
            apel = 8;
            apel *= mangga;
            Console.WriteLine($"apel= {apel}");

            mangga = 15;
            apel = 8;
            apel /= mangga;
            Console.WriteLine($"apel= {apel}");

            mangga = 15;
            apel = 8;
            apel %= mangga;
            Console.WriteLine($"apel= {apel}");



            Console.ReadKey();

        }
        static void modulus()
        {
            int mangga, orang, hasil = 0;


            Console.Write("jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("jumlah orang = ");
            orang = int.Parse(Console.ReadLine());


            hasil = mangga % orang;
            Console.WriteLine($"hasil mangga % orang = {hasil}");
            Console.ReadKey();
        }
        static void logika()
        {
            Console.Write("Masukkan umur kamu  : ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Password : ");
            string password =Console.ReadLine();

            bool isAdult = umur > 18;
            bool isPasswordValid = password == "admin";


            if (isAdult && isPasswordValid)//&& = dua dua nya harus terpenuhi / true
            {
                Console.WriteLine("anda sudah dewasa dan password valid");
            }
            else if (isAdult && !isPasswordValid)
            {
                Console.WriteLine("Anda sudah dewasa dan password invalid");
            }
            else if (!isAdult && isPasswordValid)
            {
                Console.WriteLine("Anda belum dewasa dan Password valid");
            }
            else
            {
                Console.WriteLine("anda belum dewasa dan password tidak valid");
            }

            
        
        
        
        
        
        }

        
    }
}
