--CREATE DATABASE--
--DDL
create database db_kampus;

use db_kampus;

create table mahasiswa
(
id bigint primary key identity (1,1),
nama varchar (50)not null,
alamat varchar(50) not null,
email varchar (255) null,
)

--create view--
create view vwMahasiswa 
as 
select id,nama,alamat,email from mahasiswa
--query nya disini

--select view
select * from vwMahasiswa

--alter add colomn menambah column table
alter table Biodata add [mahasiswa_id] varchar (255)
alter table Biodata alter column dob date not null

--alter drop colomn menghapus column
alter table mahasiswa drop column [description];

--table alter column
alter table mahasiswa alter column email varchar(100)

--drop view
drop view vwMahasiswa


-------------------------------------------------
--drop database
drop database 

--drop table 
drop table [namaTable]

--drop view
drop view [NamaView]

-------------------------------------
--DML

--insert data atau menambahkan data

insert into mahasiswa (nama,alamat,email) 
values ('Marchel','Medan2','marselaja@gmail.com')
	   --('Toni','Garut','toni@gmail.com'),
	   --('isni','Cimahi','isni@gmail.com')


--select data
select id,nama,alamat,email from mahasiswa
select * from mahasiswa 

--update data 
update mahasiswa set email='ilhamrizki3004@gmail.com' where id = 4
update mahasiswa set panjang = 48 where id =22
update mahasiswa set panjang =86 where id =23
update mahasiswa set panjang =117 where id =24



--Delete data
delete from mahasiswa where nama ='isni Dwitriniardi'
--membuat table
create table Biodata
(
id bigint primary key identity (1,1),
dob datetime not null,
kota varchar(100)
)
--insert table
insert into Biodata (dob,kota,mahasiswa_id)
values ('2000-03-15','DaanMogot',1),
	   ('1998-08-05','Kalibata',2),
	   ('1999-04-30','Cengkareng',4)

--menambahkan table 
alter table Biodata add mahasiswa_id bigint
alter table Biodata alter column mahasiswa_id bigint not null
--menampilkan table
select * from mahasiswa
select * from Biodata

--join table mahasiswa dan biodata
select mhs.id,mhs.nama , bio.kota,bio.dob, MONTH(bio.dob)BulanLahir,YEAR(bio.dob)TahunLahir
from mahasiswa as mhs
join Biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'ilham rizki'

-- OR terpenuhi salah satu
select mhs.id,mhs.nama , bio.kota,bio.dob, MONTH(bio.dob)BulanLahir,YEAR(bio.dob)TahunLahir
from mahasiswa as mhs
join Biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'ilham rizki' or bio.kota = 'jakarta'

--AND harus terpenuhi dua duanya
select mhs.id,mhs.nama , bio.kota,bio.dob, MONTH(bio.dob)BulanLahir,YEAR(bio.dob)TahunLahir
from mahasiswa as mhs
join Biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.nama = 'ilham rizki' and bio.kota = 'cengkareng'

--order by
--default ascending = lebih keci
--descending = lebih besar
select * from mahasiswa mhs 
join Biodata bio on mhs.id = bio.mahasiswa_id
order by mhs.id asc,mhs.nama desc

--select top
select top 2 * from mahasiswa
order by nama desc

--Between data
select * from mahasiswa where id between 1 and 3
select * from mahasiswa where id >= 1 and id <=3

--Pencocokan data LIKE :
select * from mahasiswa where nama like'm%'--menampilkan yang huruf awal M
select * from mahasiswa where nama like'%i' --menampilkan yang huruf akhir I
select * from mahasiswa where nama like'&or%'-- menampilkan karakter yang ada riz
select * from mahasiswa where nama like'_a%'--menampilkan a pada karakter kedua mArcel
select * from mahasiswa where nama like'__n%'--menampilkan a pada karakter ketiga toNi
select * from mahasiswa where nama like't_%'--
select * from mahasiswa where nama like't__%'
select * from mahasiswa where nama like't%i'

--GroupBy data
select SUM(id),nama from mahasiswa group by nama -- id nama yang sama Di SUM

select count(id),nama from mahasiswa group by nama -- nama yang sama di jumlah kan

--having 
select COUNT(id),nama 
from mahasiswa
group by nama
	having count (id) > 1

--distinct menghilangkan yagn samaa atau duplicate
select distinct nama from mahasiswa

--substring (query dimulai dari 1)
select substring ('sql Tutorial ',1,3) as judul
--charIndexs menghitung indeks ke berapa
select CHARINDEX ('t', 'Costumer') as judul
--datalenght menghitung jumlah karakter
select DATALENGTH('akumau.istirahat')

--case when 
alter table mahasiswa add panjang smallint 
select * from mahasiswa order by panjang asc --dari yang terkecil
select * from mahasiswa order by panjang desc -- dari yang terbesar

select id,nama,alamat,panjang,
	case when panjang < 50 then 'pendek'
	when panjang <= 100 then 'sedang'
	else 'Tinggi'
	end as TinggiBadan 
  from mahasiswa 

  --concat
select CONCAT('SQL ','Is ',' FUN')
select 'SQL ' + 'Is' + ' FUN'

