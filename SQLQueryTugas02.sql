---TUGAS SQL DAY 02

create database DB_Entertainer;

use DB_Entertainer

create table artis
(
id bigint primary key identity (1,1),
Kd_artis varchar(100)not null,
Nm_artis varchar (100) not null,
jk varchar (80) not null,
bayaran int not null,
award int not null,
negara varchar(100)not null
)
select * from artis

insert into artis (Kd_artis,Nm_artis,jk,bayaran,award,negara)
values('A001','ROBERT DOWNEY JR','PRIA',3000000000,2,'AS'),
	  ('A002','ANGELINA JOLIE','WANITA',700000000,1,'AS'),
	  ('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
	  ('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
	  ('A005','CHELSEA ISLAN','WANITA',300000000,0,'ID')

	  alter table artis alter column bayaran decimal(20,10)
	  alter table artis alter column bayaran decimal(20)

create table film
(
id bigint primary key identity (1,1),
Kd_film varchar(55)not null,
Nm_film varchar (55) not null,
genre varchar (50) not null,
artis varchar (55) not null,
produser varchar(55) not null,
pendapatan bigint not null,
nominasi varchar (55)
)
select * from film

insert into film(Kd_film,Nm_film,genre,artis,produser,pendapatan,nominasi)
values('F001','IRON MAN','G001','A001','PD01',2000000000,3),
	  ('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
	  ('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
	  ('F004','AVENGER CIVIL WAR','G001','A001','PD01',2000000000,1),
	  ('F005','SPIDER MAN HOMECOMING','G001','A001','PD01',1300000000,0),
	  ('F006','THE RAID','G001','A004','PD03',800000000,5),
	  ('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
	  ('F008','HABIBIE AND AINUN','G004','A005','PD03',670000000,4),
	  ('F009','POLICE STORY','G001','A003','PD02',700000000,3),
	  ('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
	  ('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
	  ('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
	  ('F009','KUNGFU PANDA','G003','A003','PD05',923000000,5)

	  alter table film alter column nominasi int
	  update film set Kd_film='F013' WHERE id =13

create table produser
(
id bigint primary key identity (1,1),
Kd_produser varchar(50)not null,
Nm_produser varchar (50) not null,
internasional varchar (50) not null,
)

insert into produser (Kd_produser,Nm_produser,internasional)
values
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')
select * from produser

create table negara
(
id bigint primary key identity (1,1),
Kd_negara varchar(100)not null,
Nm_negara varchar (100) not null,
)

insert into negara (Kd_negara,Nm_negara)
values	('AS','AMERIKA SERIKAT'),
		('HK','HONGKONG'),
		('ID','INDONESIA'),
		('IN','INDIA')
select * from negara

create table genre
(
id bigint primary key identity (1,1),
Kd_genre varchar(50)not null,
Nm_genre varchar (50) not null,
)

insert into genre (Kd_genre,Nm_genre)
values ('G001','ACTION'),
	   ('G002','HOROR'),
	   ('G003','COMEDY'),
	   ('G004','DRAMA'),
	   ('G005','THRILLER'),
	   ('G006','FICTION')
	   
select * from genre
select * from artis
select * from produser
select * from film
select * from negara
--soal 

--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan
select produser.Nm_produser,sum(film.pendapatan) as jumlahPendapatan from film
join produser on film.produser = produser.Kd_produser
where produser.Nm_produser ='MARVEL'
group by produser.Nm_produser
--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
select film.Nm_film,film.nominasi 
from film
where film.nominasi ='0' 

--3. Menampilkan nama film yang huruf depannya 'p'
select film.Nm_film
from film 
where Nm_film like 'P%'


--4. Menampilkan nama film yang huruf terakhir 'y'
select film.Nm_film
from film 
where Nm_film like '%y'

--5. Menampilkan nama film yang mengandung huruf 'd'
select film.Nm_film
from film 
where Nm_film like '%d%'

--6. Menampilkan nama film dan artis
select film.Nm_film,artis.Nm_artis from film
join artis on film.artis = artis.Kd_artis


--7. Menampilkan nama film yang artisnya berasal dari negara hongkong
select film.Nm_film,artis.negara from film 
join artis on film.artis =artis.Kd_artis
where negara ='HK' 



--8.Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'
select film.Nm_film,artis.negara from film
join artis on film.artis = artis.Kd_artis
where negara.Nm_negara not like'%o'

--9. Menampilkan nama artis yang tidak pernah bermain film
select artis.Nm_artis
